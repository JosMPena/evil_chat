class ComponentGenerator < Rails::Generators::Base
  argument :component_name, required: true, desc: 'Component name, e.g: button'

  def create_view_file
    create_with_extension 'html.erb'
  end

  def create_css_file
    create_with_extension 'css'
  end

  def create_js_file
    create_with_extension('js') do
      "import \"./#{component_name}.css\";\n"
    end
  end
  
  protected

  def create_with_extension(extension, &block)
    underscore = extension.include?('html') ? '_' : ''
    create_file(
      "#{path}/#{underscore + component_name}.#{extension}",
      &block
    )
  end

  def path
    "frontend/components/#{component_name}"
  end
end
