class Message < ApplicationRecord
  def recent?
    created_at > Time.now - 24.hours
  end
end
