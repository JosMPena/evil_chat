class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # Tell controllers to lookup views on frontend folder
  prepend_view_path Rails.root.join('frontend')
end
